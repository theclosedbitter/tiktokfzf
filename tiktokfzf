#!/bin/sh

# Requires yt-dlp, fzf, mpv or vlc
# By default, videos loop like on TikTok.
TIKTOKFZF_VERSION=2.0

# Dependency check
dep_ck () {
	for Dep; do
		if ! command -v "$Dep" 1>/dev/null; then
			printf "%s not found. Please install it.\n" "$Dep" >&2
			exit 2
		fi
	done
	unset Dep
}
dep_ck "yt-dlp" "fzf"

# Choose your video player of choice. Default is mpv.
# Alternatives include vlc or its CLI, cvlc.
# On Mac, VLC's executable is /Applications/VLC.app/Contents/MacOS/VLC
TIKTOKFZF_PLAYER="mpv"

user="$1"

if [ -z "$1" ]; then
	echo "Error. Please include the channel handle." && exit
fi

useragent=${useragent-'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.152 Safari/537.36'}
TIKTOKFZF_HELP () {
	while IFS= read -r Line; do
		printf "%s\n" "$Line";
	done <<EOF
Usage: tiktokfzf [OPTIONS...] <TikTok username>

  OPTIONS:
  -h, --help, --help-all     
Print this help message
  -v, --version
Print tiktokfzf's version number.
  -c
Skips scraping, only uses cached files.
  -f, --following
Scrape and watch already cached channel.
  -d
Downloads video only using cached files.
  -g
Scrapes TikTok, then downloads video.
  -p
Purge all downloaded video data.
  -r
Remove selected cached channel.
  -s
Search same user again immediately after playing.
EOF
}

mkdir -p "$HOME/.config/tiktokfzf"
# Download video information. Downloads are cached
TIKTOKFZF_SCRAPE () {
	echo "Scraping TikTok User $user. This may take a while."
	yt-dlp --abort-on-error --get-filename --progress "https://www.tiktok.com/@$user" > "$HOME"/.config/tiktokfzf/"$user".txt
}

TIKTOKFZF_QUERY () {
	query=$(fzf -e < "$HOME/.config/tiktokfzf/$user".txt)
	if [ -z "$query" ]; then
		echo "Error: You must pick a valid choice."
		exit
	fi
	tiktokvideoid=$(echo "$query" | cut -d "[" -f2 | cut -d "]" -f1)
}
TIKTOKFZF_SEARCH () {
	TIKTOKFZF_QUERY "$user"
	if [ -z "$tiktokvideoid" ]; then
		echo "Please choose a video."
		exit
	else
		echo "Playing $query..."
		# Due to TikTok's API and limitations with ffmpeg's interactions with mpv,
		# yt-dlp needs to be run again to grab the actual video URL.
		# If this is not done, there is a large delay for playing videos in mpv.
		# vlc also cannot interpret normal TikTok URLs.
		# --loop in both mpv and vlc for looping, like in TikTok.
		case "$TIKTOKFZF_PLAYER" in
			mpv)
				"$TIKTOKFZF_PLAYER" --loop --title="$query" "$(yt-dlp --abort-on-error --get-url "https://www.tiktok.com/@$user/video/$tiktokvideoid")";;
			vlc|cvlc|VLC)
				"$TIKTOKFZF_PLAYER" --loop --no-video-title-show "$(yt-dlp --abort-on-error --get-url "https://www.tiktok.com/@$user/video/$tiktokvideoid")";;
			*)
				echo "Please use a supported video player."
		esac
	fi
}

TIKTOKFZF_DOWNLOAD () {
	TIKTOKFZF_QUERY "$user"
	if [ -z "$tiktokvideoid" ]; then
		echo "Error. Please choose a video."
		exit
	else
		echo "Downloading $query..."
		yt-dlp "https://www.tiktok.com/@$user/video/$tiktokvideoid"
	fi
}

while getopts ":cdfghpPrRsv:" option; do
	user=$2
	case "$option" in
		c)
			unset "$user"
				subscription=$(find "$HOME/.config/tiktokfzf" -maxdepth 1 -name "*.txt" | fzf)
				user=$(basename "${subscription%.*}")
			TIKTOKFZF_SEARCH "$user"
			exit;;
		d)
			TIKTOKFZF_DOWNLOAD "$user"
			exit;;
		g)
			TIKTOKFZF_SCRAPE "$user"
			TIKTOKFZF_DOWNLOAD "$user"
			exit;;
		h|help|help-all)
			TIKTOKFZF_HELP
			exit;;
		f|following)
			unset "$user"
				subscription=$(find "$HOME/.config/tiktokfzf" -maxdepth 1 -name "*.txt" | fzf)
				user=$(basename "${subscription%.*}")
			TIKTOKFZF_SCRAPE "$user"
			TIKTOKFZF_SEARCH "$user"
			exit;;
		R)
			echo "Are you sure you want to delete your TikTok cache?"
			read -r purge
			if [ "${purge}" = y ]; then
				rm -r "$HOME/.config/tiktokfzf/"
			else
				echo "Aborting."
				exit
			fi;;
		r|remove)
			subscription=$(find "$HOME/.config/tiktokfzf" -maxdepth 1 -name "*.txt" | fzf)
			echo "Removing $subscription..."
			rm "$subscription"
			echo "$subscription data removed!"

			exit;;
		s)
			TIKTOKFZF_SCRAPE "$user"
			TIKTOKFZF_SEARCH "$user"
			if [ -z "$tiktokvideoid" ]; then
				exit
			else
				unset "$query"
				unset "$tiktokvideoid"
			TIKTOKFZF_SEARCH "$user"
			fi;;
		v|version)
			echo "tiktokfzf - Version $TIKTOKFZF_VERSION"
			exit;;
		/?)
			echo "tiktokfzf: Invalid option."
			TIKTOKFZF_HELP
			exit;;
	esac
done
if [ -z "$2" ]; then
	TIKTOKFZF_SCRAPE "$user"
	TIKTOKFZF_SEARCH "$user"
fi

# Remove accidental blank files.
# Sloppy, but effective.
rm "$HOME/.config/tiktokfzf/.txt" > /dev/null 2>&1
