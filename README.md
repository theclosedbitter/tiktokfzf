![tiktokfzf logo](https://gitlab.com/riseandfalloftechnews/media/-/raw/3e7d5fd909571af30615294b7358f9a853671218/tiktok.gif) 
_A POSIX script to search TikTok videos by username (without API) and opens/downloads videos using mpv/yt-dlp_

[Watch this video for a demo.](https://youtu.be/mHZvcHU1exw)

# Dependencies

There are only 2 dependencies that are required as well as your [video player of choice](#Supported Video Players)

## Required

* [yt-dlp](https://github.com/yt-dlp/yt-dlp), aka, [the y word that must not be named](https://lukesmith.xyz/deletion)
* [fzf](https://github.com/junegunn/fzf)

## Supported Video Players

* [mpv (Recommended)](https://mpv.io/)
* [VLC](https://www.videolan.org/vlc/)

# Install
1. Install above dependencies.
2. Run following commands

## Linux
1. ```git clone https://gitlab.com/riseandfalloftechnews/tiktokfzf```
2. ```chmod +x $PATH/tiktokfzf```
3. ```./tiktokfzf ...```

## Mac (M1 & Intel)
1. Install dependencies using [Homebrew](https://brew.sh)

```brew install yt-dlp fzf mpv```

2. Create and gain ownership of your root ```$PATH```

```sudo mkdir -p /usr/local/bin && sudo chown ${USER}:staff /usr/local/bin```

3. Log out and log back in for group permissions to take effect.
4. Download tiktokfzf and make it executable:

```curl --fail --output /usr/local/bin/tiktokfzf https://gitlab.com/riseandfalloftechnews/tiktokfzf/-/raw/master/tiktokfzf && chmod +x /usr/local/bin/tiktokfzf```

5. ```tiktokfzf ...```

# Examples
tiktokfzf saves all user queries as username.txt files in ```$HOME/.config/tiktokfzf```. This is to conserve bandwidth and store channel posts offline.

# General

> Search 

```tiktokfzf linustech```

> Search TikTok channel again immediately after playing.

```tiktokfzf -s deeptomcruise```

> Search TikTok channel using cached data.

```tiktokfzf -c```

> Search saved TikTok channel using fzf and scrape TikTok for new videos.

```tiktokfzf -f```

## Downloading

> Scrape TikTok and download selected video.

```tiktokfzf -g techlore```

> Download TikTok video using cached file.

```tiktokfzf -d murasakisweetpotatoes```

# Bugs

* Users who have been banned or accounts that don't exist will turn up empty.

* Flatpak versions of mpv or VLC have not been tested.

* Users with lots of videos will take longer to load every time they are called.

# Wishlist

* Tag search

* Raw search

* Douyin support

* Comments
